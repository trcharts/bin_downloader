FROM python:3.8
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
WORKDIR /src
ADD ./requirements.txt ./requirements.txt
RUN pip install -r ./requirements.txt
