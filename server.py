import time
from concurrent import futures

import grpc
from binance.client import Client

from proto import trcharts_pb2
from proto import trchartsbin_pb2_grpc


class TrchartsbinServicer(trchartsbin_pb2_grpc.TrchartsbinServicer):
    def Ping(self, request, context):
        res = trcharts_pb2.Text()
        res.value = "pong from python"
        return res

    def SubIndividualTicker(self, request, context):
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')

    def GetKLines(self, request, context):
        client = Client('', '')
        r = {'symbol': request.symbol, 'interval': request.interval, 'start_str': "1 day ago UTC"}
        if request.start_time.ToMicroseconds() != 0:
            r['start_str'] = request.start_time.ToJsonString()
        if request.end_time.ToMicroseconds() != 0:
            r['end_str'] = request.end_time.ToJsonString()
        if request.limit != 0:
            r['limit'] = request.limit
        klines = client.get_historical_klines(**r)
        res = trcharts_pb2.KLines()
        for k in klines:
            kline = trcharts_pb2.KLine()
            kline.symbol = request.symbol
            kline.open_time.FromMilliseconds(k[0])
            kline.open = k[1]
            kline.high = k[2]
            kline.low = k[3]
            kline.close = k[4]
            kline.volume = k[5]
            kline.close_time.FromMilliseconds(k[6])
            kline.quote_volume = k[7]
            kline.trades_count = k[8]

            res.klines.append(kline)

        return res


if __name__ == "__main__":
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    # use the generated function `add_CalculatorServicer_to_server`
    # to add the defined class to the server
    trchartsbin_pb2_grpc.add_TrchartsbinServicer_to_server(
        TrchartsbinServicer(), server)

    # listen on port 50051
    print('Starting server. Listening on port 50053.')
    server.add_insecure_port('[::]:50053')
    server.start()

    # since server.start() will not block,
    # a sleep-loop is added to keep alive
    try:
        while True:
            time.sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)
